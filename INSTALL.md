Installation
============

On a Debian Stretch installation:

```bash
SRV_PATH="/srv"
VENV_PATH="/srv/venv"

# main dependencies
apt install python3 nginx git uwsgi uwsgi-plugin-python3 python3-virtualenv python3-pip

# source code
cd $SRV_PATH
git clone https://gitlab.com/iggdrasil/ishtar-pergamon.git

# virtualenv
mkdir $VENV_PATH
virtualenv --system-site-packages $VENV_PATH/pergamon
. $VENV_PATH/pergamon/bin/activate
pip install -r $SRV_PATH/ishtar-pergamon/requirements.txt

cd $SRV_PATH/ishtar-pergamon
# initialize DB, static files, translations...
cp pergamon/settings/local.py.sample pergamon/settings/local.py
vim pergamon/settings/local.py  # at least set a SECRET_KEY, BASE_URL and ALLOWED_HOSTS

chown -R www-data:www-data media
cp Makefile.example Makefile
make collectstatic
make migrate
make compilemessages
./manage.py createsuperuser

# webserver configuration
cp conf/nginx.conf.template /etc/nginx/sites-available/pergamon.conf
vim /etc/nginx/sites-available/pergamon.conf  # change at least server name
ln -s /etc/nginx/sites-available/pergamon.conf /etc/nginx/sites-enabled/pergamon.conf

cp conf/uwsgi.ini.template /etc/uwsgi/apps-available/pergamon.ini
vim /etc/uwsgi/apps-available/pergamon.ini  # change at least projectdomain
ln -s /etc/uwsgi/apps-available/pergamon.ini /etc/uwsgi/apps-enabled/pergamon.ini
systemctl restart uwsgi nginx
```

