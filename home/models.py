from unidecode import unidecode

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from wagtail.admin.edit_handlers import FieldPanel
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField
from wagtail.images.edit_handlers import ImageChooserPanel


class BasePage(Page):
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.SET_NULL, related_name='+',
        help_text=_(
            "For top page: full width image. For child page: vignette."
        ), blank=True, null=True
    )
    body = RichTextField(blank=True)

    content_panels = Page.content_panels + [
        ImageChooserPanel('image'),
        FieldPanel('body', classname="full"),
    ]

    class Meta:
        abstract = True

    def get_homepage(self):
        page = self
        while page:
            if hasattr(page, "homepage"):
                return page.homepage
            page = self.get_parent()

    def get_logo(self):
        if hasattr(self, 'logo'):
            return self.logo
        homepage = self.get_homepage()
        if homepage:
            return homepage.get_logo()

    def get_footer(self):
        if hasattr(self, 'footer'):
            return self.footer
        homepage = self.get_homepage()
        if homepage:
            return homepage.get_footer()

    def get_context(self, request):
        context = super().get_context(request)
        homepage = self.get_homepage()
        if homepage:
            context['menu_items'] = homepage.get_children().filter(
                show_in_menus=True).live().order_by('pk')
        context["tracking_code"] = settings.PERGAMON_TRACKING_CODE
        return context

    def get_reversed_children(self):
        return reversed(self.get_children().live())

    def save(self, *args, **kwargs):
        self.slug = unidecode(self.slug)
        super().save(*args, **kwargs)


class HomePage(BasePage):
    organization = models.CharField(max_length=200, blank=True, null=True)
    logo = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.SET_NULL, related_name='+',
        blank=True, null=True
    )
    jumbotron = RichTextField(blank=True)
    footer = RichTextField(blank=True)
    content_panels = \
        [BasePage.content_panels[0]] + [
            FieldPanel('organization'),
            ImageChooserPanel('logo'),
            FieldPanel('jumbotron'),
        ] + BasePage.content_panels[1:] + [
            FieldPanel('footer'),
        ]

