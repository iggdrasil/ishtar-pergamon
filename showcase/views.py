from django.http import Http404, HttpResponse
from django.template import TemplateDoesNotExist
from django.template.loader import get_template
from django.utils.translation import ugettext_lazy as _

from .models import ShowCase


def convert_dict(value):
    """
    Convert key in each tree to replace "-" with "_"
    """
    if isinstance(value, dict):
        new_dict = {}
        for k in value.keys():
            new_dict[k.replace("-", "_")] = convert_dict(value[k])
        return new_dict
    elif isinstance(value, (list, tuple)):
        new_lst = []
        for v in value:
            new_lst.append(convert_dict(v))
        return new_lst
    return value


def display_item(request, slug, number):
    q = ShowCase.objects.filter(slug=slug)
    number = int(number)
    number -= 1  # human page number to computer page number
    if not q.count():
        return Http404(_("Unknown source."))
    showcase = q.all()[0]
    source = showcase.external_source
    template_name = "showcase/item-{}.html".format(source.source_type.slug)
    try:
        template = get_template(template_name)
    except TemplateDoesNotExist:
        raise Http404(
            str(_("Template {} is not defined. Ask your administrator to "
                  "define a template for this source type.")).format(
                template_name))
    data = showcase.get_context(request)
    data_source = source.get_item(number)
    if not data_source:
        raise Http404(
            str(_("Data unavailable"))
        )
    data["data"] = convert_dict(data_source)
    data["page"] = showcase
    data_list = showcase.data
    data_list_count = len(data_list)
    if number:
        previous = source.get_item(number - 1)
        if previous:
            data["previous"] = previous
            data["previous_number"] = number  # human number
    if number < data_list_count:
        next_item = source.get_item(number + 1)
        if next_item:
            data["next"] = next_item
            data["next_number"] = number + 2  # human number
    data["current_number_1"] = number + 1
    data["total"] = data_list_count
    return HttpResponse(template.render(data, request))

