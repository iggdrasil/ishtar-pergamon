from django.utils.translation import ugettext_lazy as _

from wagtail.contrib.modeladmin.options import (
    ModelAdmin, modeladmin_register)

from .models import ExternalSource, ExternalSourceType


class ExternalSourceTypeAdmin(ModelAdmin):
    model = ExternalSourceType
    menu_label = _("External source types")
    menu_icon = 'cogs'
    menu_order = 700
    add_to_settings_menu = True
    exclude_from_explorer = True
    list_display = ('name', 'slug')


modeladmin_register(ExternalSourceTypeAdmin)


class ExternalSourceAdmin(ModelAdmin):
    model = ExternalSource
    menu_label = _("External sources")
    menu_icon = 'site'
    menu_order = 400
    add_to_settings_menu = False
    exclude_from_explorer = False
    list_display = ('name', 'source_type', 'error')
    search_fields = ('name',)


modeladmin_register(ExternalSourceAdmin)
