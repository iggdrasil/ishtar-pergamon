from django.template import Library

register = Library()


@register.filter
def showcase_item_number(loopcounter, page):
    return loopcounter + (page - 1) * 10
