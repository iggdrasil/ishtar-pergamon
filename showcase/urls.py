from django.conf.urls import url

from .views import display_item

urlpatterns = [
    url(r'^display-item/(?P<slug>[\w-]+)/(?P<number>\d+)/$',
        display_item, name='display-item'),
]