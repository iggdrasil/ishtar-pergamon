from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '*woiq$=w@joi+vb7-d951_xd9$-85et(0fmlh&^rnz_u#@(p61'

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ['*'] 

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

EXTRA_APP = ""

try:
    from .local import *
except ImportError:
    pass

if EXTRA_APP:
    STATICFILES_DIRS += [
        os.path.join(BASE_DIR, EXTRA_APP, 'static'),
    ]
