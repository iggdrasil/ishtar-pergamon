from .base import *

DEBUG = False

try:
    from .local import *
except ImportError:
    pass

if EXTRA_APP:
    STATICFILES_DIRS += [
        os.path.join(BASE_DIR, EXTRA_APP, 'static'),
    ]
